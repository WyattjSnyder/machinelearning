import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
import pickle
from PIL import Image
import tensorflow as tf


def draw_images(images, num_images):
    w, h = 28, 28
    plt.gray()

    for i in range(num_images):
        plt.imshow(np.reshape(images[i], (w, h)))
        plt.show()


def load_model(filename):
    return pickle.load(open(filename, 'rb'))


def create_and_train_model(filename, x, y):
    model = tf.keras.Sequential()
    model.add(tf.keras.Input(shape=(28 * 28,)))
    model.add(tf.keras.layers.Dense(units=256, activation='relu'))
    model.add(tf.keras.layers.Dense(units=192, activation='relu'))
    model.add(tf.keras.layers.Dense(units=128, activation='relu'))
    model.add(tf.keras.layers.Dense(units=20, activation='softmax'))
    model.compile(optimizer='sgd', loss='mse')

    model.fit(x, y, batch_size=32, epochs=10)

    pickle.dump(model, open(filename, 'wb'))


if __name__ == "__main__":
    data = np.loadtxt('input/hand_written_digits/train.csv', delimiter=',', skiprows=1)

    x = data[:, 1:]
    y = data[:, 0]

    filename = 'digit_recognizer_logistic_regression_model.sav'

    create_and_train_model(filename, x, y)

    model = load_model(filename)
    my_image = Image.open('input/hand_written_digits/my_digit.png').convert('L')
    my_image_array = np.asarray(my_image).flatten()

    print(model.predict([my_image_array]))
