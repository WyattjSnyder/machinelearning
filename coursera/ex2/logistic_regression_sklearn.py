import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style="white")


def ex1():
    data = np.genfromtxt("ex2data1.txt", delimiter=',')
    X = data[:, :-1]
    y = data[:, -1]

    poly = PolynomialFeatures(2)
    mapped_X = poly.fit_transform(X)
    model = LogisticRegression().fit(mapped_X, y)

    xx, yy = np.mgrid[0:100:.01, 0:100:.01]
    grid = np.c_[xx.ravel(), yy.ravel()]

    probs = model.predict_proba(poly.fit_transform(grid))[:, 1].reshape(xx.shape)

    f, ax = plt.subplots()

    contour = ax.contourf(xx, yy, probs)
    f.colorbar(contour)
    ax.scatter(X[:, 0], X[:, 1], c=y[:], cmap="RdBu")
    plt.show()


def ex2():
    data = np.genfromtxt("ex2data2.txt", delimiter=',')
    X = data[:, :-1]
    y = data[:, -1]

    poly = PolynomialFeatures(6)
    mapped_X = poly.fit_transform(X)
    model = LogisticRegression(penalty='none').fit(mapped_X, y)

    xx, yy = np.mgrid[-1:1.2:.01, -1:1.2:.01]
    grid = np.c_[xx.ravel(), yy.ravel()]

    probs = model.predict_proba(poly.fit_transform(grid))[:, 1].reshape(xx.shape)

    f, ax = plt.subplots()
    contour = ax.contourf(xx, yy, probs)
    f.colorbar(contour)

    ax.scatter(X[:, 0], X[:, 1], c=y, cmap="RdBu")
    plt.show()


if __name__ == "__main__":
    ex1()
    ex2()
