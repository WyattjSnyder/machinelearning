import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import normalize
import math


def plot_data(x, y, theta):
    x_min = list(map(min, zip(*x)))[1]
    x_bound = list(map(max, zip(*x)))[1]
    linx = np.linspace(x_min, x_bound, 100)
    liny = theta[0] + linx * theta[1]
    plt.scatter([val[1] for val in x], [val[2] for val in x], c=y)
    plt.plot(linx, liny, '-r')
    plt.show()


def sigmoid(z):
    return 1 / (1 + math.exp(-z))


def vector_sum(x, theta):
    res = 0
    for i in range(len(theta)):
        res = res + x[i] * theta[i]

    return res


def cost_function(x, y, theta):
    error = 0
    for i in range(len(x)):
        predict = sigmoid(vector_sum(x[i], theta))
        error = error + (-y[i] * math.log(predict) - ((1 - y[i]) * math.log(predict)))

    return error / len(x)


def gradient_descent(x, y, theta, learn_rate):
    new_theta = []
    for i in range(len(theta)):
        error = 0
        for j in range(len(x)):
            predict = sigmoid(vector_sum(x[i], theta))
            error = error + (predict - y[j]) * x[j][i]
        curr_theta = theta[i] - (learn_rate / len(x)) * error

        new_theta.append(curr_theta)

    return new_theta


def plot_error(error):
    x = range(len(error))

    plt.plot(x, error)
    plt.show()


def predict(val, theta):
    return sigmoid(vector_sum(val, theta))


if __name__ == "__main__":
    data = np.genfromtxt("ex2data1.txt", delimiter=',')
    x = [i[:-1] for i in data]
    y = [i[-1] for i in data]

    norm_data = normalize(x, axis=0)
    norm_data = [np.insert(val, 0, 1) for val in norm_data]
    theta = np.zeros(len(x[0]))
    plot_data(norm_data, y, theta)
    curr_cost = cost_function(x, y, theta)

    for i in range(len(norm_data)):
        print(f"prediction: {predict(norm_data[i], theta)}  ----- Ans: {y[i]}")
    print("#" * 50)

    learn_rate = 0.1
    iterations = 1500
    error = []
    for i in range(iterations):
        theta = gradient_descent(norm_data, y, theta, learn_rate)
        error.append(cost_function(norm_data, y, theta))

    curr_cost = cost_function(x, y, theta)

    plot_error(error)
    plot_data(norm_data, y, theta)

    print("#" * 50)
    for i in range(len(norm_data)):
        print(f"prediction: {predict(norm_data[i], theta)}  ----- Ans: {y[i]}")
