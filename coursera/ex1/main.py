import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import normalize


def eyematrix(x):
    mat = []
    for i in range(x):
        mat.append([0] * x)

    for i in range(x):
        mat[i][i] = 1

    return mat


def plot_data(x, y, theta):
    x = [val[1] for val in x]
    plt.scatter(x, y)
    linx = np.linspace(0, 23, 100)
    liny = theta[0] + linx * theta[1]
    plt.plot(linx, liny, '-r')
    plt.show()


def cost_function(x, y, theta):
    m = len(x)
    error = 0
    for i in range(m):
        prediction = 0
        for j in range(len(theta)):
            prediction = prediction + theta[j] * x[i][j]
        error = error + (prediction - y[i]) ** 2

    error = (1 / (2 * m)) * error

    return error


def gradient_descent(x, y, theta, learn_rate):
    new_theta = []
    for i in range(len(theta)):
        error = 0
        for j in range(len(x)):
            prediction = 0
            for k in range(len(theta)):
                prediction = prediction + theta[k] * x[j][k]
            error = error + (prediction - y[j]) * x[j][i]
        curr_theta = theta[i] - (learn_rate / len(x)) * error

        new_theta.append(curr_theta)

    return new_theta


def single_variable_main():
    print(eyematrix(4))

    data = np.genfromtxt("ex1data1.txt", delimiter=',')
    x = [[1, i[0]] for i in data]
    y = [i[1] for i in data]
    theta = [0, 0]
    plot_data(x, y, theta)

    print(cost_function(x, y, theta))

    learn_rate = 0.01
    iterations = 1500

    for i in range(iterations):
        theta = gradient_descent(x, y, theta, learn_rate)
        if i % 100 == 0:
            plot_data(x, y, theta)

    plot_data(x, y, theta)

    print(cost_function(x, y, theta))


def multi_variable_main():
    data = np.genfromtxt("ex1data2.txt", delimiter=',')
    x = [i[:-1] for i in data]
    y = [i[-1] for i in data]

    norm_data = normalize(x, axis=0)

    norm_data = [np.insert(val, 0, 1) for val in norm_data]
    theta = np.zeros(len(norm_data[0]))

    for i in range(len(norm_data)):
        prediction = 0
        for j in range(len(theta)):
            prediction = prediction + norm_data[i][j] * theta[j]
        print(f"prediction: {prediction} ----- actual: {y[i]}")

    print("-" * 100)
    curr_cost = cost_function(norm_data, y, theta)
    prev_cost = curr_cost
    print(curr_cost)

    learn_rate = 1
    iterations = 15000

    for i in range(iterations):
        theta = gradient_descent(norm_data, y, theta, learn_rate)
        if i % 100 == 0:
            prev_cost = curr_cost
            curr_cost = cost_function(norm_data, y, theta)
            print(curr_cost)
            if prev_cost < curr_cost:
                print("!" * 50)
                print("cost went up")
                print("!" * 50)

    curr_cost = cost_function(norm_data, y, theta)
    print(curr_cost)

    print("-" * 100)
    for i in range(len(norm_data)):
        prediction = 0
        for j in range(len(theta)):
            prediction = prediction + norm_data[i][j] * theta[j]
        print(f"prediction: {prediction} ----- actual: {y[i]}")


if __name__ == "__main__":
    multi_variable_main()
    # single_variable_main()
